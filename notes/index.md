
## Hello :smile:

The following are course notes for CSC428. Some of these may need some
work (and some of the links may be broken because it slipped my mind to fix
them). If you find any mistakes, please don't hesitate to submit an issue or
pull request on this repo's [github](https://github.com/isthisnagee/CSC428)

## Table of Contents

* Readings 
    * [Psychology as a Science of Design](psychology-as-a-study-of-design.html)
    - [test](test.html)
