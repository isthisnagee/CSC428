---
animation:
  - timeline 
prev:
  link: index.html
  title: Home 
---

# Title

## The Emergence of Usability

Some reasons for the success of HCI include:

- It evokes many difficult problems and looks for elegant solutions
- It deals with societal and cultural views on computers and information technology
- It produces result that can be great for business.

### Software Psychology

The body of work referred to as "Software Psychology" in the 70s was focused on
establishing the use of design while developing software, encluding
a behavioral approach to software design and considering human characteristics
when developing software.

It included two methodological axioms:

1. An assumption on the validity of the waterfall method for developing software. 
2. An assumption of the following roles for psychology:
  - To produce general description of humans interacting with the system/software.
  - To directly verify the usability of systems/software as they were
    developed. The author notes that this verification usually happened after
    software was developed. (I think this has a lot to do with the waterfall
    method, since I dont believe it necessarily requires a bunch of working
    products while developing, just a finished product at the end of the
    'Development' phase). 

Issues with these axioms:

1. The author notes that the waterfall method is "infeasible and ineffective", and a "crude management tool for very large-scale, long-term projects".
2. On the assumptions for psychology:
  - Researchers produced general guidelines that were shoehorned into systems that needed specific use cases.
  - The 'general descriptions' produces were anything but general, and tended to focus on unrepresentative groups, like small programs for large scale software, undergrads standing in for 'real' programmers, etc), so researchers generally understood little about the 'real' users.

Software Psychology posed two problems for the field in the 1980s:

1. Describe design and development and understand how it can be supported
2. Specify the role that psychology (specifically social and behavioral science) should play in HCI.

### Iterative Development 

<div class="bg-light-blue w2 h2"></div>

Brooks, project manager of the IBM 360 OS said that designers should "plan to
throw away".

Shifted usability focus from summative to formative.

- The goal of formative assessment is to gather
  feedback that can be used to guide
  improvements in the design.
- The goal of summative assessment is to measure the success of a design after
  a design (or product) is completed.

<small>
These two definitions were adapted from [here](http://www.cmu.edu/teaching/assessment/howto/basics/formative-summative.html)
</small>


### User Models

The cornerstone of the effort to directly verify usability of systems/software
as it was developed was the GOMS project (Goals, Operators, Methods, and
Selection Rules). 

#### GOMS:

- Explicitly integrated many components of skilled performance to produce
  predictions about real tasks
- Useful in domains where user performance efficiency is critical.

Issues with GOMS:

- It doesn't describe _learning_.
- Does not address non routine problem solving and error.
  - New users spend $\frac{1}{3}$ to $\frac{1}{2}$ of their time in error
    recovery.

#### The Learning Problem

Coordinating knowledge between the _task_ and the _device_. Users must learn the
mappings between the _**goals** in the task domain_ and the _**actions** in the user
domain_.

Payne and Green (1989) argued for "consistency" of commands (pull and push, as
opposed to pull and grab), but Grudin questioned it, stating

> Designers striving for user interface consistency can resemble Supreme Court justices trying to define pornography: each of us feels we know it when we see it, but people often disagree and a precise definition remains elusive. A close examination suggests that consistency is an unreliable guide and that designers would often do better to focus on users’ work environments.

##### Prior Knowledge

New users tried to understand computers by using analogies to physical and
familiar things. Because of this, we have UI "metaphors" and a paradigm for UI
called "direct manipulation", an HCI style which involves continuous
representation of objects of interest and rapid, reversible, and incremental
actions and feedback.
[Wikipedia](https://en.wikipedia.org/wiki/Direct_manipulation_interface#cite_note-1)

People also want to learn by doing, but because of this they can run into weird
errors and might come up with incorrect inferences and understandings. Because
of this, the concept of the "active user" as well as a design for "learn by
doing" systems came about. This is somewhat an alternative to GOMS.


## User-Centered System Development

### Usability Engineering

### Design Rationale

### Cooperative Activity

## Synthesizing a Discipline

# The Timeline

<div class="w-100"></div>
<div id="timeline"></div>

<script>

const timeline = [
  {
    img: "img/science-of-the-artificial.jpg",
    year: { start: 1969 },
    description:
      "This book predates HCI, but two themes echo out in HCI: (1) We should expect that the technology we create will structure human behavior and experience, (2) A science of design is necessary",
    title: "The Sciences of the Artificial",
    subtitle: "no way"
  },
  {
    year: { start: 1970 },
    description:
      "The historical foundation of HCI is grounded in Software Psychology",
    // will link to `/img/hello.png`
    title: "Software Psychology"
  },
  {
    year: { start: 1970, end: 1980 },
    description: "Empirical studies began to analyze the difficulties of the waterfall method",
    title: "Fall of Waterfall"
  },
  {
    color: "light-blue",
    year: { start: 1985 },
    description: "Tanner & Buxton demonstrate issues in UI management system development, demonstrate uses of prototyping early and often (TODO: check this stmt)",
    title: "Prototyping"
  },
  {
    color: "light-blue",
    year: { start: 1983 },
    description: "Gould & Boies, Pava, and Nygaard (in '79) encourage user participation in design",
    title: "User Participation"
  },
  {
    color: "light-blue",
  year: { start: 1983 },
  description: '"Thinking aloud" was pionered in 1965 by deGroot and Newell & Simon while studying puzzles and games, was shown to illuinate strategic though in 1985, and became the "central empirical, formative evaluatoin method in HCI" in the 80s',
  title: "Thinking aloud"
  },
  {
  year: { start: 1983 },
  title: "GOMS",
  description: "The Goals, Operators, Methods, and Selectoin rules (GOMS) project provided a framework for analyzing the goals, methods, and actions that compormise human computer interactions"
  },
  {
  img: "img/acm.jpg",
  year: { start: 1988 },
  title: "ACM",
  description: "The ACM (Assoc. for Computing Machinery) listed HCI as a core area of computer science"
  }
];
</script>
