(function() {
  function timelineSort(eventA, eventB) {
    // TODO currently only by year, in future, make it a `.date`, and the type a
    // `Date` object
    if (eventA.year.start === eventB.year.start) return 0;
    return eventA.year.start > eventB.year.start ? 1 : -1;
  }

  const timelineLocation = document.getElementById("timeline");
  const sortedTimeline = timeline.sort(timelineSort);
  const timelineView = sortedTimeline.map(timelineEvent => {
    const createElement = (elem, className, textContent = "") => {
      const e = document.createElement(elem);
      e.className = className;
      e.textContent = textContent;
      return e;
    };
    const createContainer = (elem, className, ...children) => {
      const e = createElement(elem, className);
      children.forEach(child => e.appendChild(child));
      return e;
    };

    const img = createElement("img", "db w-100 br2 br--top");
    if (timelineEvent.img) img.src = timelineEvent.img;

    const bgColor = timelineEvent.color ? `bg-${timelineEvent.color}` : "";
    return createContainer(
      "div",
      `mh2 dib br2 ba dark-gray b--black-10 mv4 w-100 w-50-m w-25-l mw5 ${bgColor}`,
      img,
      createContainer(
        "div",
        "pa2 ph3-ns pb3-ns",
        createContainer(
          "div",
          "dt w-100 mt1",
          createContainer(
            "div",
            "dtc",
            createElement("h1", "f5 f4-ns mv0", timelineEvent.title)
          ),
          createContainer(
            "div",
            "dtc tr",
            createElement(
              "date",
              "f5 mv0",
              timelineEvent.year.end
                ? `${timelineEvent.year.start} - ${timelineEvent.year.end}`
                : timelineEvent.year.start
            )
          )
        ),
        createElement(
          "p",
          "f6 lh-copy measure mt2 mid-gray",
          timelineEvent.description
        )
      )
    );
  });

  timelineView.forEach(eventView => timelineLocation.appendChild(eventView));
  timelineLocation.className = "db";
})();
